// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::path::PathBuf;

error_chain! {
    errors {
        /// The queue directory is not actually a directory.
        NotADirectory(path: PathBuf) {
            description("not a directory")
            display("not a directory: {:?}", path)
        }

        /// A handler already exists with the given name.
        DuplicateHandler(kind: String) {
            description("duplicate handler")
            display("duplicate kind: {}", kind)
        }

        /// A handler already exists with the given name.
        HandlerError {
            description("handler error")
        }
    }
}
