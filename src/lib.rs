// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]

//! A small library to dispatch job files to relevant handlers.
//!
//! This library implements the core logic of a `Director` which dispatches jobs described in JSON
//! files to a relevant `Handler` and then archives the job based on whether it was accepted or
//! rejected.
//!
//! # Job files
//!
//! Job files are files ending in a `.json` extension in JSON format. Two keys are required:
//!
//!   - `kind`: this string value is used to determine which handler will be used to handle the
//!     job.
//!   - `data`: this value is passed to the handler.
//!
//! Other keys may be used (e.g., a `timestamp` to indicate when the job was created).
//!
//! Job files are treated as read-only by the director.

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate log;

mod director;
mod error;
mod handler;
mod handlers;
mod watcher;
pub mod utils;

pub use director::Director;
pub use director::RunResult;
pub use error::Error;
pub use error::ErrorKind;
pub use error::Result;
pub use error::ResultExt;
pub use handler::Handler;
pub use handler::HandlerResult;
pub use handlers::DirectorWatchdog;

#[cfg(test)]
mod test;
