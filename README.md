JSON Job Dispatch
=================

A library which watches a directory using inotify for `.json` job files and
dispatches them to registered handlers. Jobs are archived into `accept/` and
`reject/` directories for auditing purposes. Additionally, companion files with
a datestamp of when the job was handled and, for rejected jobs, a file
containing the reason for the rejection is also created.

The design goal is to allow the daemon handling jobs to crash and be resiliant
against crashes by using the filesystem as a simple object store for jobs which
need handled and an archive of completed jobs. Atomic renames are used to
relocate files, so the input directory must reside on the same filesystem as
the archival directories.
